const mongoose = require("mongoose");

const projectSchema = mongoose.Schema({
  title: {
    type: String,
    required: [true, "Project should have a title"],
  },
  description: {
    type: String,
    required: [true, "Project shoul have description"],
  },
  amount: {
    type: long,
    required: [true, "Project must have description"],
  },
  created: {
    type: Date,
    default: Date.now,
  },
  updated: {
    type: Date,
    default: Date.now,
  },
  imageName: {
    type: String,
    required: true,
  },
  image: {
    data: Buffer,
    contentType: String,
  },
});

module.exports = mongoose.model("Project", projectsSchema);