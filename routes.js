const Blog = require("../projectModel");

const router = async function (req, res) {
  //  GET: /api/projects
  if (req.url === "/api/projects" && req.method === "GET") {
    // get the projects.
    const projects = await Projet.find();
    // set the status code, and content-type
    res.writeHead(200, { "Content-Type": "application/json" });
    // send the data
    res.end(JSON.stringify(projects));
  }

  //  GET: /api/projects/:id
  if (req.url.match(/\/api\/projects\/([0-9]+)/) && req.method === "GET") {
    try {
      // extract id from url
      const id = req.url.split("/")[3];
      // get project from DB
      const projects = await Projects.findById(id);

      if (project) {
        // set the status code and content-type
        res.writeHead(200, { "Content-Type": "application/json" });
        // send the data
        res.end(JSON.stringify(project));
      } else {
        throw new Error("Requested project does not exist");
      }
    } catch (error) {
      // set the status code and content-type
      res.writeHead(404, { "Content-Type": "application/json" });
      // send the error
      res.end(JSON.stringify({ message: error }));
    }
  }

  //  POST: /api/projects
  if (req.url === "/api/projects" && req.method === "POST") {
    try {
      let body = "";

      //listen for data event
      req.on("data", (chunk) => {
        body += chunk.toString();
      });

      //Listen for end event
      req.on("end", async () => {
        //Create Project instance
        let project = new Project(JSON.parse(body));

        //Save instance to DB
        await project.save();

        // set the status code and content-type
        res.writeHead(200, { "Content-Type": "application/json" });

        //send response
        res.end(JSON.stringify(project));
      });
    } catch (error) {
      console.log(error);
    }
  }

  //PUT: /api/projects/:id
  if (req.url.match(/\/api\/projects\/([0-9]+)/) && req.method === "PUT") {
    try {
      // extract id from url
      const id = req.url.split("/")[3];

      let body = "";

      //listen for data event
      req.on("data", (chunk) => {
        body += chunk.toString();
      });

      req.on("end", async () => {
        let updatedProject = await Project.findByIdAndUpdate(id, JSON.parse(body), {
          new: true,
        });

        // set the status code and content-type
        res.writeHead(200, { "Content-Type": "application/json" });
        //send response
        res.end(JSON.stringify(updatedProject));
      });
    } catch (error) {
      console.log(error);
    }
  }

  // DELETE: /api/projects/:id
  if (req.url.match(/\/api\/projects\/([0-9]+)/) && req.method === "DELETE") {
    try {
      // extract id from url
      const id = req.url.split("/")[3];
      // delete project
      await Project.findByIdAndDelete(id);
      // set the status code and content-type
      res.writeHead(200, { "Content-Type": "application/json" });
      // send the message
      res.end(JSON.stringify({ message: "Project was deleted sucessfully" }));
    } catch (error) {
      // set the status code and content-type
      res.writeHead(404, { "Content-Type": "application/json" });
      // send the error
      res.end(JSON.stringify({ message: error }));
    }
  }
    //  POST: /api/upload
  if (req.url === "/api/upload" && req.method === "POST") {
    try {
      let body = "";

      //listen for data event
      req.on("data", (chunk) => {
        body += chunk.toString();
      });

      //Listen for end event
      req.on("end", async () => {
        //Create Project instance
        let project = new Project(JSON.parse(body));

        //Save instance to DB
        await project.save();

        // set the status code and content-type
        res.writeHead(200, { "Content-Type": "application/json" });

        //send response
        res.end(JSON.stringify(project));
      });
    } catch (error) {
      console.log(error);
    }
  }  
  //  POST: /api/projects
  if (req.url === "/api/projects" && req.method === "POST") {
    try {
      let body = "";

      //listen for data event
      req.on("data", (chunk) => {
        body += chunk.toString();
      });

      //Listen for end event
      req.on("end", async () => {
        //Create Project instance
        let project = new Project(JSON.parse(body));

        //Save instance to DB
        await project.save();

        // set the status code and content-type
        res.writeHead(200, { "Content-Type": "application/json" });

        //send response
        res.end(JSON.stringify(project));
      });
    } catch (error) {
      console.log(error);
    }
  }
};

module.exports = router;